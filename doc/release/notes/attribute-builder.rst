Attribute Builder (Python)
--------------------------

A Python class :py:class:`smtk.attribute_builder.AttributeBuilder` was added
to support Python operation tracing. The class has a method
:py:meth:`build_attribute()` for editing attribute contents from an input
specification (dictionary). Details are provided at
`smtk/doc/userguide/attribute/attribute-builder.rst`.
