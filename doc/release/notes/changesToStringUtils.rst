Added toBoolean to StringUtils
------------------------------

The new method will convert a string to a boolean.  If the method was successful it will return true else it will return false.  All surround white-space is ignored and case is ignored.

Current valid  values for true are: t, true, yes, 1
Current valid  values for false are: f, false, no, 0
