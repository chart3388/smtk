//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef smtk_common_TypeTraits_h
#define smtk_common_TypeTraits_h

namespace smtk
{
namespace common
{

template<class...>
using void_t = void;

}
} // namespace smtk

#endif // smtk_common_TypeTraits_h
